from sys import path, argv, exit
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QDialog, QMainWindow
# from upload import Ui_MainWindow as UIUpload
# from upload_newproblem import Ui_MainWindow as UIUploadNewProblem
# from upload_CookieEnable import Ui_MainWindow as UICheakCookie
# from edit import addproblem,cheakEnable,modproblem
from wana_Decypt0r import Ui_MainWindow as UI
from PyQt5.QtGui import QColor, QImage, QLinearGradient, QPainter,  QPixmap
# from PyQt5 import QtGui
from PyQt5.QtCore import QSize, Qt
from change_wallpaper import set_wallpaper_from_bmp
from putpicture import put


class MyWindow(QMainWindow, UI):
    def drawLinearVer(self, painter):
        lg = QLinearGradient(340,245,355,395)
        lg.setColorAt(0, Qt.green)
        lg.setColorAt(1, QColor(219,5,0))

        painter.setBrush(lg)
        painter.setPen(Qt.transparent)
        painter.drawRect(340,245,30,150)

        painter.setPen(Qt.white)
    
    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing, True)
        
        #绘制边框线
        painter.drawRect(self.rect())
        # self.setFixedSize(168, 168)
        
        self.drawLinearVer(painter)

    def __init__(self, parent=None):
        super(MyWindow, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(QtCore.Qt.WindowCloseButtonHint)
        self.setWindowTitle("wana Decrypt0r")
        self.setStyleSheet(
            'QMainWindow{background-color:rgb(128,0,0)}'
            'QLabel{color:rgb(255,255,255)}'

            "QPushButton{color:black}"
            # "QPushButton:hover{color:red}"
            "QPushButton{background-color:rgb(244,244,244)}"
            "QPushButton{border:2px}"
            # "QPushButton{border-radius:10px}"
            "QPushButton{padding:2px 4px}"
        )

        # self.oops.setTextColor()

        img = QImage(path[0]+'\\icon.ico')
        newWidth = 190
        newHeight = 190  # 缩放宽高尺寸
        size = QSize(newWidth, newHeight)
        pixImg = QPixmap.fromImage(img.scaled(size, Qt.IgnoreAspectRatio))
        self.label.setPixmap(pixImg)


        

    # def paintEvent(self, a0: QtGui.QPaintEvent) -> None:
    #     painter = QPainter(self)
    #     painter.setBrush(Qt)
    #     painter.drawRect(self.rect())


if __name__ == '__main__':
    put()
    set_wallpaper_from_bmp('D:\\wallpaper.bmp')
    app = QApplication(argv)
    myWin = MyWindow()
    myWin.show()
    exit(app.exec_())
