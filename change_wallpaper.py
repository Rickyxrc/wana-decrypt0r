#coding:utf8
#环境要求:python2.7x,PIL,pywin32
#备注:只在win7系统试过正常
#创建时间:2015-1
 
# import PIL.Image as Image
from posixpath import abspath
from win32api import RegOpenKeyEx,RegSetValueEx
from win32con import HKEY_CURRENT_USER,KEY_SET_VALUE,REG_SZ
from os import path,chdir
from sys import path
 
def set_wallpaper_from_bmp(bmp_path):
    reg_key = RegOpenKeyEx(HKEY_CURRENT_USER,"Control Panel\\Desktop",0,KEY_SET_VALUE)
    RegSetValueEx(reg_key, "Wallpaper", 0, REG_SZ, bmp_path)

if __name__ == '__main__':
    set_wallpaper_from_bmp((path[0])+'\\wallpaper.bmp')